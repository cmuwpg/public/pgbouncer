FROM debian:bullseye-slim

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    pgbouncer postgresql-client

USER 70
